package org.cys.commands;

import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.logging.BotLogger;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class HelloCommand extends BotCommand {

    private static final String LOGTAG = "HELLOCOMMAND";
    private LocalTime time = LocalTime.now();
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
    private DateTimeFormatter formatToOnlyHour = DateTimeFormatter.ofPattern("HH");
    private DateTimeFormatter formatToOnlyMinutes = DateTimeFormatter.ofPattern("mm");

    public HelloCommand() {
        super("jp2", "Sprawdź ile czasu pozostało do śmierci papieża");
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] arguments) {

        String currentTime = "Obecnie jest " + time.format(formatter) + ".";
        SendMessage timeInformation = getSendMessage(chat, currentTime);

        String messageTextBuilder;
        if (compareTime()) {
            messageTextBuilder = "Papież nie żyje!";
            SendMessage answer = getSendMessage(chat, messageTextBuilder);
            try {
                absSender.execute(answer);
                absSender.execute(timeInformation);
            } catch (TelegramApiException e) {
                BotLogger.error(LOGTAG, e);
            }
        }
        messageTextBuilder = "Papież żyje.";

        SendMessage answer = getSendMessage(chat, messageTextBuilder);
        try {
            absSender.execute(answer);
            absSender.execute(timeInformation);
        } catch (TelegramApiException e) {
            BotLogger.error(LOGTAG, e);
        }
    }

    private SendMessage getSendMessage(Chat chat, String messageTextBuilder) {
        SendMessage answer = new SendMessage();
        answer.setChatId(chat.getId().toString());
        answer.setText(messageTextBuilder);
        return answer;
    }

    private boolean compareTime() {
        String currentTimeInHours = time.format(formatToOnlyHour);
        String currentTimeInMinutes = time.format(formatToOnlyMinutes);
        int currentTimeToIntHour = Integer.parseInt(currentTimeInHours);
        int currentTimeToIntMinutes = Integer.parseInt(currentTimeInMinutes);
        int jp2TimeHours = 21;
        int jp2TimeSeconds = 37;

        return (currentTimeToIntHour == jp2TimeHours && currentTimeToIntMinutes == jp2TimeSeconds);
    }

 //TODO - metoda do konkretnych obliczen
}