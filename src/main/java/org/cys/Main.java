package org.cys;

import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;


public class Main {
    public static void main(String[] args) {
        ApiContextInitializer.init();

        TelegramBotsApi botsApi = new TelegramBotsApi();

        try {
            botsApi.registerBot(new BeznazwyBot2(BotConfig.COMMANDS_USER));
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
        System.out.println("BeznazwyBot successfully started!");
    }
}