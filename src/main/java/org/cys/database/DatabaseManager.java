package org.cys.database;

import org.telegram.telegrambots.meta.logging.BotLogger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DatabaseManager {
    private static final String LOGTAG = "DATABASEMANAGER";

    private static volatile DatabaseManager instance;
    private static volatile ConnectionDB connetion;

    private DatabaseManager() {
        connetion = new ConnectionDB();
        final int currentVersion = connetion.checkVersion();
        BotLogger.info(LOGTAG, "Current db version: " + currentVersion);
        if (currentVersion < CreationStrings.version) {
            recreateTable(currentVersion);
        }
    }

    /**
     * Get Singleton instance
     *
     * @return instance of the class
     */
    public static DatabaseManager getInstance() {
        final DatabaseManager currentInstance;
        if (instance == null) {
            synchronized (DatabaseManager.class) {
                if (instance == null) {
                    instance = new DatabaseManager();
                }
                currentInstance = instance;
            }
        } else {
            currentInstance = instance;
        }
        return currentInstance;
    }

    public boolean getUserStateForCommandsBot(Integer userId) {
        int status = -1;
        try {
            final PreparedStatement preparedStatement = connetion.getPreparedStatement("Select status FROM CommandUsers WHERE userId=?");
            preparedStatement.setInt(1, userId);
            final ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                status = result.getInt("status");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return status == 1;
    }

    private void recreateTable(int currentVersion) {
        try {
            connetion.initTransaction();
            if (currentVersion == 0) {
                currentVersion = createNewTables();
            }
            if (currentVersion == 1) {
                currentVersion = updateToVersion2();
            }
            if (currentVersion == 2) {
                currentVersion = updateToVersion3();
            }
            if (currentVersion == 3) {
                currentVersion = updateToVersion4();
            }
            if (currentVersion == 4) {
                currentVersion = updateToVersion5();
            }
            if (currentVersion == 5) {
                currentVersion = updateToVersion6();
            }
            if (currentVersion == 6) {
                currentVersion = updateToVersion7();
            }
            if (currentVersion == 7) {
                currentVersion = updateToVersion8();
            }
            connetion.commitTransaction();
        } catch (SQLException e) {
            BotLogger.error(LOGTAG, e);
        }
    }


    private int updateToVersion2() throws SQLException {
        connetion.executeQuery(CreationStrings.createRecentWeatherTable);
        connetion.executeQuery(String.format(CreationStrings.insertCurrentVersion, 2));
        return 2;
    }

    private int updateToVersion3() throws SQLException {
        connetion.executeQuery(CreationStrings.createDirectionsDatabase);
        connetion.executeQuery(String.format(CreationStrings.insertCurrentVersion, 3));
        return 3;
    }

    private int updateToVersion4() throws SQLException {
        connetion.executeQuery(CreationStrings.createLastUpdateDatabase);
        connetion.executeQuery(String.format(CreationStrings.insertCurrentVersion, 4));
        return 4;
    }

    private int updateToVersion5() throws SQLException {
        connetion.executeQuery(CreationStrings.createUserLanguageDatabase);
        connetion.executeQuery(String.format(CreationStrings.insertCurrentVersion, 5));
        return 5;
    }

    private int updateToVersion6() throws SQLException {
        connetion.executeQuery(CreationStrings.createWeatherStateTable);
        connetion.executeQuery(CreationStrings.createUserWeatherOptionDatabase);
        connetion.executeQuery(CreationStrings.createWeatherAlertTable);
        connetion.executeQuery(String.format(CreationStrings.insertCurrentVersion, 6));
        return 6;
    }

    private int updateToVersion7() throws SQLException {
        connetion.executeQuery("ALTER TABLE WeatherState MODIFY chatId BIGINT NOT NULL");
        connetion.executeQuery(String.format(CreationStrings.insertCurrentVersion, 7));
        return 7;
    }

    private int updateToVersion8() throws SQLException {
        connetion.executeQuery(CreationStrings.CREATE_COMMANDS_TABLE);
        connetion.executeQuery(String.format(CreationStrings.insertCurrentVersion, 8));
        return 8;
    }

    private int createNewTables() throws SQLException {
        connetion.executeQuery(CreationStrings.createVersionTable);
        connetion.executeQuery(CreationStrings.createFilesTable);
        connetion.executeQuery(String.format(CreationStrings.insertCurrentVersion, CreationStrings.version));
        connetion.executeQuery(CreationStrings.createUsersForFilesTable);
        connetion.executeQuery(CreationStrings.createRecentWeatherTable);
        connetion.executeQuery(CreationStrings.createDirectionsDatabase);
        connetion.executeQuery(CreationStrings.createUserLanguageDatabase);
        connetion.executeQuery(CreationStrings.createWeatherStateTable);
        connetion.executeQuery(CreationStrings.createUserWeatherOptionDatabase);
        connetion.executeQuery(CreationStrings.createWeatherAlertTable);
        connetion.executeQuery(CreationStrings.CREATE_COMMANDS_TABLE);
        return CreationStrings.version;
    }


}
